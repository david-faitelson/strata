module strata.segments {
	requires strata.pages;
	requires strata.persistent;
	requires org.junit.jupiter.api;
	requires java.logging;
	requires strata.logging;
	exports strata.segments.iface;
}