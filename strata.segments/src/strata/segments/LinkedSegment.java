package strata.segments;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.UUID;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import strata.pages.iface.Page;
import strata.pages.iface.PageManager;
import strata.persistent.PersistentInputMedium;
import strata.persistent.PersistentOutputMedium;
import strata.persistent.PersistentEntity;
import strata.segments.iface.Segment;

public class LinkedSegment extends PersistentEntity implements Segment {

	static final int LAYER = 2;

	private static Logger logger = Logger.getLogger("strata.segments");

	private UUID id;
	private Vector<Page> pages;
	private LinkedSegment next;
	private LinkedSegment prev;
	
	private void log(String message) {
		logger.log(Level.FINE, message, LAYER);
	}

	public LinkedSegment() {
		pages = new Vector<Page>();
		id = UUID.randomUUID();
	}

	public LinkedSegment(UUID aUUID) {
		pages = new Vector<Page>();
		id = aUUID;
	}
	
	@Override
	public void append(Integer value) {
		
		if (pages.lastElement().isFull())
			pages.add(PageManager.instance().createPage());
		
		pages.lastElement().append(value);
		
		log("appended " + value + " to segment " + id() );
	}

	@Override
	public Integer at(Integer index) {
		
		Integer result = pages.elementAt( index / Page.PAGE_SIZE).at(index % Page.PAGE_SIZE);
		
		log("accessed segment " + id + " at index " + index);
		
		return result;
	}

	@Override
	public Integer size() {
		return (pages.size() - 1) * Page.PAGE_SIZE + pages.lastElement().size(); 
	}

	@Override
	public LinkedSegment splitAt(Integer anInteger) {
		
		assert anInteger >= 0;
		assert anInteger <= size();
		
		if (anInteger == 0)
			return this;
		
		if (anInteger == size())
			return this;
		
		Integer index = anInteger / Page.PAGE_SIZE;
		
		Integer offset = anInteger % Page.PAGE_SIZE;
		
		LinkedSegment left = new LinkedSegment();

		LinkedSegment mid = new LinkedSegment();
		
		LinkedSegment right = new LinkedSegment();
		
		for(int i = 0 ; i < index ; i++) {
			left.pages.add(pages.elementAt(i));
		}
		
		if (offset == 0) {
			right.pages.add(pages.elementAt(index));
		}
		else {
			Page[] ps = pages.elementAt(index).splitAt(offset);
			left.pages.add(ps[0]);
			mid.pages.add(ps[1]);
		}	
		
		for(int i = index + 1; i < pages.size(); i++) {
			right.pages.add(pages.elementAt(i));
		}
		
		prev.linkTo(left);
		
		if (offset == 0) {
			left.linkTo(right);
		} else {
			left.linkTo(mid);
			mid.linkTo(right);
		}
		
		if (offset > 0 && index == pages.size() - 1) {
			mid.linkTo(next);
		} else {
			right.linkTo(next);
		}

		if (offset == 0) {
			log("splitted segment " + id + " at index " + index + " to segments " + left.id() + ", " + right.id() + " (left -> right split)");
		} else if (offset > 0 && index < pages.size() - 1) {
			log("splitted segment " + id + " at index " + index + " to segments " + left.id() + ", " + mid.id() + " (left -> mid split)");
		} else {
			log("splitted segment " + id + " at index " + index + " to segments " + left.id() + ", " + mid.id() + "," + right.id() + " (left -> mid -> right split)");			
		}
		
		return left;
	}

	public void linkTo(Segment aSegment) {
		next = (LinkedSegment)aSegment;
		next.prev = this;
	}

	public UUID id() {
		return id;
	}

	public Segment next() {
		return next;
	}

	public Segment prev() {
		return prev;
	}

	public LinkedSegment addFirstPage() {
		assert pages.isEmpty();
		
		pages.add(PageManager.instance().createPage());
		return this;
	}

	// file in/out
	
	@Override
	public void fileIn() {
		
		LinkedSegmentMedium.instance().load(this);
		
		log( "filed in segment " + id());
	}
	
	public void loadContentFrom(PersistentInputMedium aMedium) {
		
		try {
			pages.clear();
		
			while(aMedium.hasNext()) {
				pages.add(PageManager.instance().recreatePageWithId(aMedium.nextInteger()));
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Error in loadContentFrom: " + e.getMessage());
		}
	}
	
	@Override
	public void fileOut() {
		
		LinkedSegmentMedium.instance().store(this);
		
		log( "filed out segment " + id());
	}
	
	public void storeContentOn(PersistentOutputMedium aMedium) {
		for(Page page : pages) {
			aMedium.write(page.id());
		}
	}
	
	public void storeChildren() {
		for(Page page : pages) {
			page.fileOut();
		}
	}

	@Override
	public String visualize() {
		
		String dotCode =  "\"segment" + id()+"\"";
		
		dotCode += "[label = \"";
		
		if (pages.size() > 0) {
			dotCode += "<" + "p" + pages.get(0).id() + ">" + pages.get(0).id();
		}
		
		for(int i = 1; i < pages.size(); i++) {
			dotCode += "|" + "<" + "p" + pages.get(i).id() + ">" + pages.get(i).id();
		}

		dotCode += "\"];\n";

		for(int i = 0; i < pages.size(); i++) {
			dotCode += "\"segment" + id() + "\":" + "p" + pages.get(i).id() + " -> " + "page" + pages.get(i).id() + ";\n";  
		}

		for(int i = 0; i < pages.size();i++) {
			dotCode += pages.get(i).visualize();
		}
		
		/*if (next != null) {
			dotCode += "\"segment" + id() + "\":e -> " + "\"segment" + next.id() + "\":w;\n";
		}*/
		
		return dotCode;

	}

}
