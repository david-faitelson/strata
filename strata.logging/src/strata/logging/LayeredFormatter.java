package strata.logging;

import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class LayeredFormatter extends SimpleFormatter {

	@Override public String format(LogRecord aLogRecord) {
		
		int layer = (int)aLogRecord.getParameters()[0];
		
		String indent = "";
		
		for(int i = 0 ; i < layer;i++) indent += "  ";
		
		return layer + indent + super.format(aLogRecord);
		
	}

}
