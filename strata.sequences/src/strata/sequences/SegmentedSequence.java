package strata.sequences;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import strata.persistent.PersistentEntity;
import strata.persistent.PersistentInputMedium;
import strata.persistent.PersistentOutputMedium;
import strata.segments.iface.Segment;
import strata.segments.iface.SegmentFactory;
import strata.sequences.iface.Sequence;

public class SegmentedSequence extends PersistentEntity implements Sequence {

	static final int LAYER = 3;

	private static Logger logger = Logger.getLogger("strata.sequences");

	private static String graphVizPath;
	
	static {
		try {
			InputStream input = SegmentedSequence.class.getClassLoader().getResourceAsStream("debug.properties");
			
			Properties prop = new Properties();

			prop.load(input);
			
			graphVizPath = prop.getProperty("sequence.visfile");
			
		}
		catch(IOException ex) {
			ex.printStackTrace();
			System.exit(1);
		}
	}
	
	UUID id;
	
	private Segment head;
	private Segment tail;

	private void log(String message) {
		logger.log(Level.FINE, message, LAYER);
	}

	public SegmentedSequence() {
		
		id = UUID.randomUUID();
		
		head = SegmentFactory.instance().createSegment();
		tail = SegmentFactory.instance().createSegment();
		
		reset();
		
		log("created a new sequence " + id);
	}
	
	public SegmentedSequence(UUID aUUID) {
		id = aUUID;
		head = SegmentFactory.instance().createSegment();
		tail = SegmentFactory.instance().createSegment();
		// no need to link them as this command must always be immediately followed by a call to fileIn
	}

	private void reset() {
		Segment p = SegmentFactory.instance().createSegment();
		head.linkTo(p);
		p.linkTo(tail);
	}

	@Override
	public void append(Integer value) {

		tail.prev().append(value);

		log("appended " + value + " to sequence " + id);

	}

	@Override
	public Integer at(Integer index) {
		Map.Entry<Segment, Integer> entry = segmentOffsetAt(index);
		Integer result = entry.getKey().at(entry.getValue());

		log("accessed sequence " + id + " at index " + index);
		
		return result;

	}
	
	private Map.Entry<Segment, Integer> segmentOffsetAt(Integer index) {
		
		Integer i = index;
		Segment p = head.next();
		while (p != tail && i >= p.size())
		{
			i = i - p.size();
			p = p.next();
		}
		
		if (p == tail)
			throw new RuntimeException("index out of bounds");
		
		return new AbstractMap.SimpleEntry<Segment, Integer>(p, i);
	}

	@Override
	public void cut(Integer from, Integer to) {
		Segment b = splitAt(from);
		Segment e = splitAt(to);
		b.linkTo(e.next());
		
		log("cut sequence " + id + " from " + from + " to " + to);
	}

	private Segment splitAt(Integer index) {
		
		Map.Entry<Segment, Integer> entry = segmentOffsetAt(index);
		
		return entry.getKey().splitAt(entry.getValue());
	}

	@Override
	public void paste(Sequence aSequence, Integer index) {
		
		Segment aSegment = splitAt(index);
		
		((SegmentedSequence)aSequence).pasteAfter(aSegment);

		log("pasted sequence " + aSequence.id() + " into sequence " + id + " at " + index);
	}
	
	private void pasteAfter(Segment aSegment) {
		tail.prev().linkTo(aSegment.next());
		aSegment.linkTo(head.next());
		reset(); // clear this sequence as its content has been pasted into another sequence.
	}

	@Override
	public Integer size() {
		Integer n = 0;
		Segment p = head.next();
		while (p != tail) {
			n += p.size();
			p = p.next();
		}
		return n;
	}

	@Override
	public UUID id() {
		return id;
	}

	@Override
	public void loadContentFrom(PersistentInputMedium aMedium) {
				
		head.linkTo(tail);
		
		while(aMedium.hasNext()) {
			Segment aNewSegment = SegmentFactory.instance().recreateSegmentWithId(aMedium.nextUUID());
			tail.prev().linkTo(aNewSegment);
			aNewSegment.linkTo(tail);
		}
	}

	@Override
	public void loadChildren() {
		Segment p = head.next();
		while (p != tail) {
			p.fileIn();
			p = p.next();
		}
	}

	@Override
	public void fileIn() {
		SegmentedSequenceMedium.instance().load(this);
		
		log("filed in sequence " + id);
	}

	@Override
	public void storeContentOn(PersistentOutputMedium aMedium) {
		Segment p = head.next();
		while (p != tail) {
			aMedium.writeUUID(p.id());
			p = p.next();
		}
	}
	
	@Override 
	public void storeChildren() {
		Segment p = head.next();
		while (p != tail) {
			p.fileOut();
			p = p.next();
		}	
	}
	
	@Override
	public void fileOut() {
		SegmentedSequenceMedium.instance().store(this);		

		log("filed out sequence " + id);
	}

	@Override
	public Sequence duplicate() {
		fileOut();
		return SegmentedSequenceFactory.instance().recreateSequenceWithId(id());
	}

	@Override
	public String visualize() {
		String dotCode = "";
		
		Segment p = head;
		
		p = p.next();
		
		while (p != tail) {
			dotCode += p.visualize();
			dotCode += "\"segment" + p.id() + "\":e -> \"segment" + p.next().id() + "\":w [label=next];\n";
			dotCode += "\"segment" + p.next().id() + "\":w -> \"segment" + p.id() + "\":e [label=prev];\n";
			p = p.next();
		}
		
		return dotCode;
	}
	
	public void writeVisFile() {
		
		try {
			
			FileWriter file = new FileWriter(graphVizPath);
			file.write("digraph g {\r\n"
					+ "\r\n"
					+ "node [shape = record,height=.1];\r\n"
					+ "");
			file.write(visualize());
			file.write(""
					+ "}");
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
