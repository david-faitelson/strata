module strata.sequences {
	requires strata.segments;
	requires strata.persistent;
	requires junit;
	requires strata.logging;
}