module strata.pages {
	requires org.junit.jupiter.api;
	requires junit;
	requires java.logging;
	requires strata.persistent;
	requires strata.logging;
	exports strata.pages.iface;
}